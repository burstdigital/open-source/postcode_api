<?php

namespace Drupal\postcode_api\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\salesforce\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "address_resource",
 *   label = @Translation("Address resource"),
 *   uri_paths = {
 *     "canonical" = "/postcode-api/address"
 *   }
 * )
 */
class AddressResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('postcode_api'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {

    /**
     * First, get the API key from the settings form.
     */
    $config = \Drupal::config('postcode_api.configuration');

    $api_key = $config->get('api_key');
    if(empty($api_key)) {
      throw new ServiceUnavailableHttpException(60*60, 'API Key not set');
    }

    /**
     * Next, build the query
     */
    $postcode = \Drupal::request()->get('postcode', '');
    if(empty($postcode)) {
      throw new BadRequestHttpException('Please provide a postcode.');
    }

    $number = \Drupal::request()->get('number', '');

    $url = 'https://postcode-api.apiwise.nl/v2/addresses';

    try {
      $api_result = \Drupal::httpClient()->get($url, [
        'headers' => [
          'X-Api-Key' => $api_key,
        ],
        'query' => [
          'postcode' => $postcode,
          'number' => $number,
        ]
      ]);
    }
    catch(\GuzzleHttp\Exception\RequestException $e) {
      throw new BadRequestHttpException(json_decode($e->getResponse()->getBody()->getContents())->error);
    }

    $api_result_decoded = json_decode($api_result->getBody(), TRUE);

    $addresses = $api_result_decoded['_embedded']['addresses'];

    if(empty($addresses)) {
      throw new NotFoundHttpException('No address found.');
    }

    $address = $addresses[0];

    unset($address['_links']);

    $response = new ResourceResponse($address);
    $response->addCacheableDependency(['#cache' => ['max-age' => 0]]);

    return $response;
  }

}
